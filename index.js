const TelegramBot = require('node-telegram-bot-api');
const puppeteer = require('puppeteer');
var cron = require('node-cron');
const CREDS = require('./creds');
const Halacha = require('./models/halacha');
const moment = require('moment');
const mongoose = require('mongoose');

mongoose.connect(CREDS.mongo, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => console.log('Connected !'))
    .catch(() => console.log('Connexion error !'));

class HalachaYomit {
    title = '🕯️ לע"נ אברהם בן עמוס 🕯️';
    extractHalacha = async () => {

        const browser = await puppeteer.launch({
            headless: true,
            args: ['--no-sandbox', '--disable-setuid-sandbox']
        });
        const page = await browser.newPage();
        await page.setViewport({
            width: 1800,
            height: 768
        });
        await page.goto('https://2halachot.org/%D7%90%D7%A8%D7%9B%D7%99%D7%95%D7%9F');
        await page.waitForSelector('.box_ask');
        await page.click('.box_ask > .archive_he:first-child');

        await page.waitForSelector('.gdlr-blog-content');
        const element = await page.$(".gdlr-blog-content");
        const text = await page.evaluate(element => element.textContent, element);
        await browser.close();
        this.halacha = text;
    }

    sentToTelegram(title, halacha) {
        const bot = new TelegramBot(CREDS.token, {
            polling: false
        });
        const msg = `${title} \n ${halacha}`;
        bot.sendMessage(CREDS.groupId, msg);
        return this;
    }

    async saveToDb() {
        const isSended = await Halacha.findOne({
            halacha: this.halacha
        });
        if (!isSended) {
            const halacha = new Halacha({
                title: this.title,
                halacha: this.halacha,
                sendAt: moment().format('YYYY-MM-DD')
            });
            halacha.save();
            this.sentToTelegram(this.title, this.halacha)
        } else {
            let search = true;
            let days = 13;
            let halachaDaysAgo;
            while (search) {
                halachaDaysAgo = await Halacha.findOne({
                    sendAt: moment().subtract(days, 'd').format('YYYY-MM-DD')
                });
                if (halachaDaysAgo) {
                    search = false
                } else {
                    days++
                }
            }
            if (halachaDaysAgo) {
                this.sentToTelegram(halachaDaysAgo.title, halachaDaysAgo.halacha)
            }
        }
    }
}
async function start() {
    let HalachaYomitObj = new HalachaYomit();
    await HalachaYomitObj.extractHalacha();
    HalachaYomitObj.saveToDb();
}

start();

cron.schedule('0 9 * * 0-5', () => {
    start()
}, {
    scheduled: true,
    timezone: "Asia/Jerusalem"
});