const mongoose = require('mongoose');

const halachaSchema = mongoose.Schema({
    title: {
        type: String,
        required: false
    },
    halacha: {
        type: String,
        required: true
    },
    sendAt: {
        type: String,
    },
});

module.exports = mongoose.model('Halacha', halachaSchema);