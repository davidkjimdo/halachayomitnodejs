# halachayomitNodeJs
halachayomitNodeJs

## Getting Started
```
npm install 
```
Or
```
yarn
```

### Prerequisites
```
npm install pm2 -g
```

Configure your credentials and link to your ad in creds.js

```
module.exports = {
    token: 'TOKEN_YOUR_BOT',
    groupId: 'GROUPID_OF_YOUR GROUP_TELEGRAM'
}
```


## Running the application
run once:

```
node index.js
```

for lunch pm2, will now start the app.js (Every day at 9AM the script run automatically)

```
pm2 start index.js
```
